from nlpia.data.loaders import get_data
import pandas as pd
from nltk.tokenize import casual_tokenize
from collections import Counter
from sklearn.naive_bayes import MultinomialNB

movies = get_data('hutto_movies')

df = pd.DataFrame.from_records([Counter(casual_tokenize(t)) for t in movies.text])
df = df.fillna(0).astype(int)


nb = MultinomialNB()
nb = nb.fit(df, movies.sentiment > 0) # True is positive, False is negative

p = nb.predict_proba(df) * 8 - 4


# the dataframe of movie reviews (sentiment, text):
print(movies)

"""
       sentiment                                               text
id                                                                 
1       2.266667  The Rock is destined to be the 21st Century's ...
2       3.533333  The gorgeously elaborate continuation of ''The...
3      -0.600000                     Effective but too tepid biopic
4       1.466667  If you sometimes like to go to the movies to h...
5       1.733333  Emerges as something rare, an issue movie that...
...          ...                                                ...
10601  -0.062500                        Well made but mush hearted.
10602  -1.500000                                     A real snooze.
10603  -0.625000                                      No surprises.
10604   1.437500  We’ve seen the hippie turned yuppie plot befor...
10605  -1.812500  Her fans walked out muttering words like ''hor...

[10605 rows x 2 columns]
"""




# The dataframe of document (movie review) vectors
print(df)

"""
       The  Rock  is  destined  to  be  the  ...  dipsticks  Bearable  Staggeringly  ’  ve  muttering  dissing
0        1     1   1         1   2   1    1  ...          0         0             0  0   0          0        0
1        2     0   1         0   0   0    1  ...          0         0             0  0   0          0        0
2        0     0   0         0   0   0    0  ...          0         0             0  0   0          0        0
3        0     0   1         0   4   0    1  ...          0         0             0  0   0          0        0
4        0     0   0         0   0   0    0  ...          0         0             0  0   0          0        0
...    ...   ...  ..       ...  ..  ..  ...  ...        ...       ...           ... ..  ..        ...      ...
10600    0     0   0         0   0   0    0  ...          0         0             0  0   0          0        0
10601    0     0   0         0   0   0    0  ...          0         0             0  0   0          0        0
10602    0     0   0         0   0   0    0  ...          0         0             0  0   0          0        0
10603    0     0   0         0   0   0    2  ...          0         0             0  2   1          0        0
10604    0     0   0         0   0   0    2  ...          0         0             0  0   0          1        1

[10605 rows x 20756 columns]
"""


# True is positive sentiment, False is negative:
print(movies.sentiment > 0)
""""
id
1         True
2         True
3        False
4         True
5         True
         ...  
10601    False
10602    False
10603    False
10604     True
10605    False
Name: sentiment, Length: 10605, dtype: bool
"""

# Predicted probabilities
print(p)
"""
array([[-2.51151475,  2.51151475],
       [-3.9999042 ,  3.9999042 ],
       [ 3.655976  , -3.655976  ],
       ...,
       [ 1.48144924, -1.48144924],
       [-3.98898791,  3.98898791],
       [ 3.99795422, -3.99795422]])
"""

# Add a column with the predicted value 
movies['prediction'] = [v[1] for v in p]
print(movies)

# Add a column with the error in each prediction vs the label
movies['error'] = (movies.prediction - movies.sentiment).abs()

# How much does this correctly predict positive sentiment?
movies['sentiment_ispositive'] = (movies.sentiment > 0).astype(int)
movies['predicted_ispositive'] = (movies.prediction > 0).astype(int)
score = (movies.predicted_ispositive == movies.sentiment_ispositive).sum() / len(movies)

print(score)
"""
0.9344648750589345
"""


# A new sentence
s = "The class is really great!"

# "vectorize" - make a series with counts for these words -- and all other words in the corpus
myseries = pd.Series(Counter(casual_tokenize(s)), index = df.columns).fillna(0)

# predict_proba wants 2D array or similar
print(pd.DataFrame([myseries]))
"""
   The  Rock   is  destined   to   be  the  ...  dipsticks  Bearable  Staggeringly    ’   ve  muttering  dissing
0  1.0   0.0  1.0       0.0  0.0  0.0  0.0  ...        0.0       0.0           0.0  0.0  0.0        0.0      0.0

[1 rows x 20756 columns]
"""

# print the prediction
print(nb.predict_proba(pd.DataFrame([myseries])))
"""
array([[0.51603855, 0.48396145]])
"""

# scale and print:
print(nb.predict_proba(pd.DataFrame([myseries])) * 8 - 4)
"""
array([[ 0.12830839, -0.12830839]])
"""

