from collections import OrderedDict, Counter
from nltk.tokenize import casual_tokenize

# cosine similarity - expects two sequences of numbers
def cosine_sim(v1, v2):
    def prod(x, y):
        return x*y
    def mag(v):
        return math.sqrt(sum([x**2 for x in v]))

    dot_prod = sum(map(prod, v1, v2))
    return dot_prod / (mag(v1) * mag(v2))


"""
>>> cosine_sim([1,0,0,1], [0,1,1,0])
0.0
>>> cosine_sim([1,0,0,1], [0,1,1,1])
0.40824829046386296
>>> cosine_sim([1,0,0,1], [1,0,1,1])
0.8164965809277259
>>> cosine_sim([1,0,0,1], [1,0,0,1])
0.9999999999999998
"""



# brief examples on OrderedDicts
print(Counter(casual_tokenize("This is a is a  sentence")))
"""
Counter({'is': 2, 'a': 2, 'This': 1, 'sentence': 1})
"""

print(OrderedDict(Counter(casual_tokenize("This is a is a  sentence"))))

"""
OrderedDict([('This', 1), ('is', 2), ('a', 2), ('sentence', 1)])
"""
print(OrderedDict(Counter(casual_tokenize("This is a is a  sentence")))['This'])
"""
1
"""

print(OrderedDict(Counter(casual_tokenize("This is a is a  sentence"))).values())
"""
dict_values([1, 2, 2, 1])
"""


print(OrderedDict(Counter(casual_tokenize("This is a is a  sentence"))).keys())
"""
dict_keys(['This', 'is', 'a', 'sentence'])
"""
