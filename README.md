# BMIG6014-Fall2021-Examples

This repository will hold code examples shown during our lecture sessions in the BMIG 6014 Fall 2021 course.

You may fork this repository or clone and use it directly, but please do not push anything into this repository.

The other repository with assignments is here:
https://bitbucket.org/bmig-6014-nlp-fall-2021/bmig6014-fall2021-assignments