import nltk
import collections 

"""
 Working with corpora and generating bigrams. 
 Based on https://www.nltk.org/book/ch02.html
 Requires NLTK
"""


nltk.download('gutenberg')

# example sentence
foxs = "the quick brown fox is a fast brown fox"
foxbg = list(nltk.bigrams(foxs.split()))



# print the bigrams
print(f"Fox bigrams: {foxbg}")
# print the bigram count
print(f"Fox bigram Counter: {collections.Counter(foxbg)}")




# get the list of "words" in Alice in Wonderland
cawords = nltk.corpus.gutenberg.words('carroll-alice.txt')




# get the bigrams
# note that nltk.bigrams returns a generator. We store the result a list so we can reuse it
cabgl = list(nltk.bigrams(cawords))


print(f"Num words in lewis carroll's alice: {len(list(cawords))}")
print(f"Num bigrams in order in lewis carroll's alice: {len(list(cabgl))}")
print(f"Num unique bigrams in lewis carroll's alice: {len(set(cabgl))}")




# generate bigrams for the words in 'Alice', and count them using
#  https://docs.python.org/3/library/collections.html#collections.Counter
c = collections.Counter(list(cabgl))

csorted = sorted(c.items(), key = lambda x : x[1], reverse = True)
print(f"The 20 most common bigrams: {csorted[:20]}")
