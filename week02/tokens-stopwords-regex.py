import pandas as pd
from nltk.tokenize import RegexpTokenizer
from nltk.tokenize import TreebankWordTokenizer
import nltk
import re

"""
Start by downloading a spreadsheet of COVID-19 abstracts from this page:
COVID-19 Research Articles
  https://www.cdc.gov/library/researchguides/2019novelcoronavirus/researcharticles.html
"""

"""
Reading & slicing the data frame
"""
# you may have this file in a different location
infile = './All_Articles_Excel_Dec2019July2020.xlsx'

df = pd.read_excel(infile, dtype = {'Abstract' : str})

print(df.shape) # what's the shape of this frame?
print(df.columns) # show the columns

df2 = df[['Title', 'Abstract']] # get a dataframe with just the title and abstract columns

# a series with abstract text for those abstracts that have text
abs = df2[~pd.isna(df2.Abstract)].Abstract 


# put the abstracts into a list (don't print this!)
alist = list(abs)

"""
Pull and tokenize one example text from the list of abstracts
"""
t = alist[13686] # the text we looked at on Thursday "Patients with diabetes mellitus have been reported..."

# simple whitespace tokenizer
print(t.split())

# Treebank word tokenizer
tbt = TreebankWordTokenizer()
print(tbt.tokenize(t))

# Regular expression tokenizer
tr = RegexpTokenizer(r'\w+|$[0-9.]|\S+')
print(tr.tokenize(t))


"""
Removing stopwords with the NLTK stopwords list

https://www.nltk.org/book/ch02.html#wordlist-corpora
"""

# Stopwords
nltk.download('stopwords')
sw = nltk.corpus.stopwords.words('english')

# Tokenize and remove stopwords
toks = tbt.tokenize(t)
print("\nTokens: \n%s" % toks)
print("\nTokens after stopword removal:\n %s" %  [w for w in toks if not w in sw])

puncts = ['.', ',', '(', ')']
print("\nTokens after stopword removal, and a cursory attempt at  punctuation removal:\n %s" % [w for w in toks if not w in sw and not w in puncts])


"""
Regular expressions 

https://regex101.com/
"""


# sample sentence 
sample = """
This is a sentence about China, which is a place where Chinese people often live.
Someone could write "china" without the capital C and we might want to find that as well.
"Chinchilla" and "chinquapin" are two other words that start with the suffix "chin".
Chinchillas are rodents usually found in South America.
Chinquapins are chestnuts.
Castenea henryi, also know as the Chinese chinquapin, is a chestnut variety native to China.
Capuchins do not usually live in China.
Capuchins are monkeys with hairy chins.
"""

print("Sample text: %s\n" % sample)

regex_res = re.findall("[Cc]hin[ae]\w*", sample)

print(f"Result of running our regular expression on sample text: {regex_res} ")


"""
Running the regular expression on the first 100 abstracts in the list.
Note some interesting results that are not what we're looking for.
"""
print("Run the regex on the first 100 abstracts in our list:")
print([re.findall("[Cc]hin[ae]\w*", a) for a in alist[:100]])

print("Run the regex on the first 100 abstracts in our list, and only show matches that are not empty:")
print([x for x in [re.findall("[Cc]hin[ae]\w*", a) for a in alist[:100]] if x])
