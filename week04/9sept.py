from nltk.tokenize import TreebankWordTokenizer
import pandas as pd
import string
import itertools
import collections

somedocs = ['Buffalo buffalo buffalo buffalo buffalo.',
            'The New England Patriots will play in Buffalo on November 11',
            'The American bison is commonly referred to as the American buffalo or simply buffalo in North America']

"""
If a and b are two sequences of numbers representing vectors, this function returns their dot product
"""
def dot(a, b):
    return sum([x*y for (x,y) in zip(a,b)])


"""
Uses a tokenizer and punctuation list to generate a list of tokens from a string
"""
def preptext(d):
    tkr = TreebankWordTokenizer()
    return [t for t in tkr.tokenize(d.lower()) if t not in string.punctuation]

"""
Use itertools to flatten a list of lists
"""
def flatten(l):
    return [x for x in itertools.chain.from_iterable(l)]


"""
Given some list of strings, get a sorted list of all the unique tokens used in all those strings
"""
def get_vocab(docs):
    return sorted(set(flatten([preptext(d) for d in docs])))


"""
Given some list of strings, get a DataFrame with frequency counts for words in those documents
"""
def freq_counts(docs):
    return pd.DataFrame([collections.Counter(preptext(d)) for d in docs], columns = get_vocab(docs))


df = freq_counts(somedocs)
# print out the first document's vector
print(df.iloc[0])
