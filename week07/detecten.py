from langdetect import detect
from langdetect import detect_langs

"""
One issue we encountered in using topic modeling this week was the presence of abstracts that langdetect 
was classifying as English, even though they used a mix of English and other languages. This is a complication 
we would like to eliminate when exploring topic modeling.

In addition to the langdetect.detect() function, there is also a detect_langs() function that provides probabilities 
for each of the languages it detects.
"""


#    >>> endf.iloc[677].Abstract

ml_abs =  """En este artículo, el autor reflexiona sobre las expectativas de los profesionales de la salud acerca de la evidencia para recomendar tratamiento farmacológico a los pacientes con COVID-19 (AU) In this article, the author reflects on the expectations of health professionals regarding the evidence to recommend pharmacological treatment to patients with COVID-19 (AU)"""

"""
>>> detect(ml_abs)
'en'
"""

langs = detect_langs(ml_abs)
print(langs)
# [en:0.5344439885686485, es:0.4655548379820134]

"""
>>> langs[0]
en:0.5344439885686485
>>> langs[0].lang
'en'
>>> langs[0].prob
0.5344439885686485
"""

print({l.lang : l.prob for l in langs}['en'])
# 0.5344439885686485

# Returns true if its input is English text and false otherwise
def is_english(text, thresh = .9):
    try:
        langs = detect_langs(text)
        return {l.lang : l.prob for l in langs}['en'] > thresh
    except:
        return False

# Old version of is_english - didn't work well with mixed language text 
def is_english_old(text):
    try:
        return detect(text) == 'en'
    except:
        return False

print(f'checking "{ml_abs}"')
print(f'is_english_old: {is_english_old(ml_abs)}')
print(f'is_english: {is_english(ml_abs)}')
