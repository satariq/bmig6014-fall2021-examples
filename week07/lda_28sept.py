import pandas as pd
import string
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import LatentDirichletAllocation as LDA
from langdetect import detect
from nltk.corpus import stopwords

infile = './All_Articles_Excel_Dec2019July2020.xlsx'
adf = pd.read_excel(infile, dtype = {'Abstract' : str})
adf = adf[~adf.Abstract.isna()]

# Make a list of stopwords
stops = stopwords.words('english') + list(string.punctuation)

# Returns true if its input is English text and false otherwise
def is_english(text):
    try:
        return detect(text) == 'en'
    except:
        return False

# get 1000 abstracts
endf = adf[['Abstract']].copy()[:1000]
# create a column indicating which abstracts are English
endf['English'] = endf.Abstract.apply(is_english)
# rebuild the df with just English abstracts
endf = endf[endf.English]

# build the vectorizer -- recall that the results are very different with/without stopwords
tfidf_model = TfidfVectorizer(stop_words = stops)
tfidf_docs = tfidf_model.fit_transform(raw_documents=endf.Abstract).toarray()

# n_components is the number of topics. Play around with this.
lda = LDA(n_components = 10) 
lda.fit(tfidf_docs)

"""
 Adapted from NLPIA - we'll explore this more on Thurs, Sept 30

Example usage: 
print_topics(lda, tfidf_model.get_feature_names())
"""
def print_topics(lda_model, vocab, n_top_words = 10):
    for topic_idx, topic in enumerate(lda_model.components_):
        print("\nTopic #%d:" % topic_idx)
        print(" ".join([vocab[i]
                        for i in topic.argsort()[:-n_top_words - 1:-1]]))
